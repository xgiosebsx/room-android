package com.example.android.roomwordssample.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.android.roomwordssample.R
import com.example.android.roomwordssample.databinding.ActivityMainBinding
import com.example.android.roomwordssample.db.WordRoomDatabase
import com.example.android.roomwordssample.db.dao.WordDao
import com.example.android.roomwordssample.db.entity.Word
import com.example.android.roomwordssample.ui.adapter.WordListAdapter
import kotlinx.coroutines.*

class MainActivity : AppCompatActivity() {

    companion object {

        private const val WORDS_SHARED_PREFERENCES = "WORDS_SHARED_PREFERENCES"
        private const val SIZE_WORD_LIST = "SIZE_WORD_LIST"
        private const val SIZE_WORD_LIST_DEF_VALUE = 0

    }

    private lateinit var binding: ActivityMainBinding

    private lateinit var dao: WordDao
    private lateinit var database: WordRoomDatabase
    private lateinit var adapter: WordListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        database = WordRoomDatabase.getDatabase(this)
        dao = database.wordDao()

        binding.buttonSave.setOnClickListener {
            saveWord()
        }

        binding.buttonDelete.setOnClickListener {
            deleteAll()
        }

        showWordList()

    }

    private fun showWordList() {
        adapter = WordListAdapter()
        binding.recyclerview.adapter = adapter
        binding.recyclerview.layoutManager = LinearLayoutManager(this)

        CoroutineScope(Dispatchers.IO).launch {
            val wordList = dao.getAlphabetizedWords()

            withContext(Dispatchers.Main) {
                adapter.submitList(wordList)
                saveWordListSize(wordList.size)
                showWordListSize()
            }
        }
    }

    private fun saveWord() {
        val wordText = binding.editWord.text.toString()
        val word = Word(wordText)

        CoroutineScope(Dispatchers.IO).launch {
            dao.insert(word)
            val wordList = dao.getAlphabetizedWords()

            withContext(Dispatchers.Main) {
                adapter.submitList(wordList)
                saveWordListSize(wordList.size)
                showWordListSize()
            }
        }
    }

    private fun deleteAll() {
        CoroutineScope(Dispatchers.IO).launch {
            dao.deleteAll()
            val wordList = dao.getAlphabetizedWords()

            withContext(Dispatchers.Main) {
                adapter.submitList(wordList)
                saveWordListSize(wordList.size)
                showWordListSize()
            }
        }
    }

    private fun showWordListSize() {
        val wordlistSize = getWordListSize()
        binding.textWordListSize.text = getString(R.string.number_of_words, wordlistSize)
    }

    private fun saveWordListSize(sizeWordList: Int) {
        val sharedPreferences = getSharedPreferences(WORDS_SHARED_PREFERENCES, MODE_PRIVATE)
        val sharedPreferencesEditor = sharedPreferences.edit()
        sharedPreferencesEditor.putInt(SIZE_WORD_LIST, sizeWordList).apply()
    }

    private fun getWordListSize(): Int {
        val sharedPreferences = getSharedPreferences(WORDS_SHARED_PREFERENCES, MODE_PRIVATE)
        return sharedPreferences.getInt(SIZE_WORD_LIST, SIZE_WORD_LIST_DEF_VALUE)
    }

}
